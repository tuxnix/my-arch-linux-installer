**Purpose:**
The Arch Linux Installlation with malis script needs a computer runnig Arch Linux and a HDD or SSD where you want to install your new Arch Linux on. Malis is not for Arch Linux beginners. All configurations and package selections will be made before you start the script. The installation process runs without any further user input. Because most packages needn't to be downloaded, installation is very fast and takes for a fully configurated system with GUI under 10 min.


**Usage:**
Do your configurations in the malis.conf file.
Start malis script.
(Both files, malis and malis.conf have to be executable.)

  Right now malis supports:
- BIOS booting computer
- fdisk partitioning
- gdisk partitioning
- Swap and none Swap partitioning
- Grub bootloader
- Sylinux bootloader

**Not supported for now:**
UEFI, encryption and LVM.

Malis is in alpha development state. It runs fine for me, but still has to be tested.
